﻿//FROM MODULE 7&8:
//Fill out the body of this class so it extends BaseRecipeCategory
//and implements IRecipeCategory

class RecipeCategory extends BaseRecipeCategory  implements IRecipeCategory{         
    public description: string;
    examples: IExample[];
    constructor(category: any) {
        super(category.name, category.foodGroups);
        this.description = category.description;
        this.examples = category.examples;
    }

} 

