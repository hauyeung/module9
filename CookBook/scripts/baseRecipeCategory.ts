﻿//FROM MODULES 6-8: 
//Copy your BaseRecipeCategory class code

class BaseRecipeCategory  {
    name: string;
    foodGroups: FoodGroup[] = [];
    constructor(_name: string, _foodGroups: FoodGroup[]) {
        this.name = _name;
        this.foodGroups = _foodGroups;
    }

  
} 